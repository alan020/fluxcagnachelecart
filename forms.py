from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import PasswordInput
from feedparser import *

class SignIn(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),])
    login = StringField('Login', validators=[DataRequired(),])
    password = StringField('Passord', widget=PasswordInput(hide_value=True))
    pass

class LogIn(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),])
    password = StringField('Password', widget=PasswordInput(hide_value=True))
    pass

class AddFlux(FlaskForm):
    lien = StringField('Flow link ', validators=[DataRequired()])
    pass
