# Fluxcagnachelecart

Projet LP RGI Web Dynamique
LECART Alan - CAGNACHE Hippolyte

## Installation

```sh
 $ pipenv install
```


## Execution

```sh
 $ pipenv shell
 (pipenv-shell)$ flask run
```


### Initialisation de la base de donnée
```sh
 (pipenv-shell)$ flask dropdb
 (pipenv-shell)$ flask initdb
```
#### /!\ Les données sont déja initialisée dans le fichier .sqlite donc pas besoin de le faire/!\

Un utilisateur est déja crée avec deux flux: 
| login   |     email    | password |
| :-----: | :----------: | :-------:|
| alan | alan.lecart@outlook.com |   alan    |


# Fonctionnement
Deux template, un pour l'instription et la connection, un 2e qui contient la barre de navigation dans le site qui sera ensuite déclinée pour l'affichage de la liste de flux, et un flux seul et la page d'ajout d'un flux.

La page feed n'affiche que les titres des flux, c'est seulement une fois qu'on clique dessus que l'on a les détails.

